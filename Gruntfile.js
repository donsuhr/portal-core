// Generated on 2013-11-27 using generator-cmcportal 0.0.0
'use strict';

module.exports = function (grunt) {
	require('load-grunt-tasks')(grunt, {
		pattern: ['grunt-*', '!grunt-template-jasmine-requirejs']
	});
	require('time-grunt')(grunt);

	var bowerrc = grunt.file.readJSON('./.bowerrc');
	var bowerDir = bowerrc.directory;
	var coreModules = require('./coreIncludes.json');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		getPackageFolder: function () {
			return this.pkg.name.charAt(0).toLowerCase() + this.pkg.name.slice(1);
		},
		yeoman: {
			app: 'app',
			dist: 'dist',
			bowerDir: bowerDir,
			coreModules: coreModules
		},
	});

	grunt.task.loadTasks('build/grunt');

	grunt.registerTask(
		'build', [
			'clean', 'less:dist', 'copy:cssconcat', 'useminPrepare', 'imagemin', 'htmlmin', 'preprocess', 'replace',
			'autoprefixer', 'concat', 'cssmin', 'usemin', 'amdWrapKendoFiles', 'requirejs', 'copy:dist', 'uglify'
		]
	);

	grunt.registerTask('dist', ['serve']);

	grunt.registerTask('serve', ['build', 'connect:dist:keepalive']);

	grunt.registerTask('test', ['qunit', 'jasmine', 'jshint', 'complexity']);

	grunt.registerTask('preview', ['clean', 'less:dist', 'qunit', 'jshint', 'connect:livereload', 'watch']);

	grunt.registerTask(
		'default', ['preview']
	);

	grunt.registerTask(
		'distStyles', ['less:dist', 'autoprefixer', 'copy:cssconcat', 'useminPrepare', 'concat', 'cssmin']
	);
};

require(['common'], function () {
	'use strict';
	require.config({
		paths: {
			'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min',
			'bootstrap': '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min'
		},
		deps: ['dist', 'jquery'],
		callback: function () {
			require(['jquery', 'main']);
		}
	});
});


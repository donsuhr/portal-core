module.exports = function (grunt) {
	grunt.config.set('connect', {
		options: {
			port: 9000,
			// change this to '0.0.0.0' to access the server from outside
			hostname: '0.0.0.0',
			livereload: 35729
		},
		livereload: {
			options: {
				open: false,
				base: [
					'.tmp', '<%= yeoman.app %>'
				]
			}
		},
		test: {
			options: {
				port: 9001,
				base: [
					'.tmp', 'test', '<%= yeoman.app %>'
				]
			}
		},
		dist: {
			options: {
				open: true,
				base: '<%= yeoman.dist %>',
				livereload: false,
				port: 9002
			}
		}
	});
};
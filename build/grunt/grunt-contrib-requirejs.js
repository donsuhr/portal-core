module.exports = function (grunt) {
	grunt.config.set('requirejs', {
		// options: https://gist.github.com/mattsahr/4190206
		// bundle examples : https://github.com/cloudchen/requirejs-bundle-examples
		compile: {
			options: {
				appDir: 'app',
				dir: '.require-build',
				mainConfigFile: '<%= yeoman.app %>/scripts/common.js',
				optimize: 'none',
				skipDirOptimize: true,
				normalizeDirDefines: 'skip',
				optimizeCss: false,
				wrap: false,
				paths: {
					'kendo': '../../<%= amdWrapKendoFiles.task.out %>'
				},
				'modules': [
					{
						'name': 'common',
						'include': '<%= yeoman.coreModules.common %>',
						// jquery and bootstrap from cdn
						'exclude': ['jquery', 'bootstrap']
					},
					{
						'name': 'common.noConfig',
						'include': '<%= yeoman.coreModules.common %>',
						// jquery and bootstrap from cdn
						'exclude': ['jquery', 'bootstrap']
					},
					{
						'name': 'main',
						'exclude': [
							'common', '<%= pkg.name %>', 'jquery', 'bootstrap'
						]
					},
					{
						'name': '<%= pkg.name %>',
						'exclude': ['common', 'jquery', 'bootstrap'],
					}
				]
			}
		}
	});
};
module.exports = function (grunt) {
	grunt.config.set('uglify', {
		dist: {
			options: {
				compress: true,
				report: 'min'
			},
			files: {
				'<%= yeoman.dist %>/scripts/<%= getPackageFolder() %>/<%= pkg.name %>.js': [
					'<%= requirejs.compile.options.dir %>/scripts/<%= getPackageFolder() %>/<%= pkg.name %>.js'
				]
			}
		}
	});
};
module.exports = function (grunt) {
	grunt.config.set('concurrent', {
		options: {
			logConcurrentOutput: true
		},
		preview: {
			tasks: ['watch:livereload', 'watch:less', 'watch:test', 'open:delayForConcurrent']
		},
		working: {
			tasks: ['watch:livereload', 'watch:less', 'open:delayForConcurrent']
		}
	});
};
module.exports = function (grunt) {
	grunt.config.set('copy', {
		cssconcat: {
			files: [
				{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '.tmp',
					src: ['styles/bootstrap-custom.css']
				}
			]
		},
		dist: {
			files: [
				{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>',
					src: [
						'*.{ico,png,txt}', '.htaccess', 'images/{,*/}*.{webp,gif}',
						'styles/fonts/{,*/}*.*',
						'bower_components/sass-bootstrap/fonts/*.*',
						'bower_components/kendo-ui/styles/kendo.common.min.css',
						'bower_components/kendo-ui/styles/kendo.default.min.css',
						'bower_components/kendo-ui/styles/Default/*.*',
						//'bower_components/bootstrap/img/*.*',
						// ie8 uses placeholder
						'bower_components/jquery-placeholder/jquery.placeholder.min.js',
						// copy all in case not included in min output
						'bower_components/kendo-ui/js/*.js',
						// using mockjax and mockjson in demo dist version
						'bower_components/mockJSON/js/jquery.mockjson.js',
						'bower_components/jquery-mockjax/jquery.mockjax.js',
						// replace uses require.min, in vendor because min not in bower dist
						'scripts/vendor/requirejs-2.1.9/require.min.js',
						'styles/ie9.css'
					]
				},
				{
					expand: true,
					dot: true,
					cwd: '<%= requirejs.compile.options.dir %>',
					dest: '<%= yeoman.dist %>',
					src: [
						'scripts/common.js',
						'scripts/main.js',
						'scripts/dist.js',
						'scripts/common.noConfig.js'
						//'scripts/<%= pkg.name %>.js' -- use uglify version
					]
				}
			]
		}
	});
};
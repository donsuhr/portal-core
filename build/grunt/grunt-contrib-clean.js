module.exports = function (grunt) {
	grunt.config.set('clean', {
		dist: {
			files: [
				{
					dot: true,
					src: [
						'.tmp', '<%= yeoman.dist %>/*',
						'<%= requirejs.compile.options.dir %>',
						'!<%= yeoman.dist %>/.git*'
					]
				}
			]
		},
		server: '.tmp'
	});
};
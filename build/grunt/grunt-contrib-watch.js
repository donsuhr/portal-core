module.exports = function (grunt) {
	grunt.config.set('watch', {
		livereload: {
			options: {
				livereload: '<%= connect.options.livereload %>'
			},
			files: [
				'.tmp/styles/**/*.css',
				'<%= yeoman.app %>/*.html',
				'<%= yeoman.app %>/templates/**/*.html',
				'<%= yeoman.app %>/scripts/**/*.js',
				'<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
			]
		},
		less: {
			files: ['<%= yeoman.app %>/styles/{,*/}*.less'],
			tasks: ['less:compile'],
			options: {
				//spawn : false
			}
		},
		test: {
			files: [
				'<%= yeoman.app %>/scripts/**/*.js',
				'test/**/*.js',
				'test/**/*.html',
				'build/**/*.js'
			],
			tasks: ['qunit', 'jshint', 'kendo_lint'],
			options: {
				//spawn : false
			}
		},
		jasmine: {
			files: [
				'<%= yeoman.app %>/scripts/**/*.js',
				'test/spec/**/*.js',
				'!<%= yeoman.app %>/scripts/vendor/**'
			],
			tasks: ['jasmine']
		}


	});
};
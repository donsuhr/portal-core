module.exports = function (grunt) {
	grunt.config.set('preprocess', {
		dist: {
			// removes @if NODE_ENV='production' block
			src: '<%= yeoman.dist %>/index.html',
			options: {
				inline: true
			}
		}
	});
};
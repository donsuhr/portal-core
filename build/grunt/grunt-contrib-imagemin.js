module.exports = function (grunt) {
	grunt.config.set('imagemin', {
		dist: {
			files: [
				{
					expand: true,
					cwd: '<%= yeoman.app %>/images',
					src: '{,*/}*.{gif,jpeg,jpg,png}',
					dest: '<%= yeoman.dist %>/images'
				}
			]
		}
	});
};
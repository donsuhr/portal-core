module.exports = function (grunt) {
	grunt.config.set('jshint', {
		options: {
			jshintrc: '.jshintrc',
			reporter: require('jshint-stylish')
		},
		gruntfile: {
			options: {
				jshintrc: '.jshintrc'
			},
			src: 'Gruntfile.js'
		},
		app: {
			options: {
				jshintrc: 'app/.jshintrc'
			},
			src: [
				'app/**/*.js', '!app/bower_components/**', '!app/scripts/vendor/**'
			]
		},
		test: {
			options: {
				jshintrc: 'test/.jshintrc'
			},
			src: ['test/**/*.js']
		},
		build: {
			options: {
				jshintrc: 'build/.jshintrc'
			},
			src: ['build/**/*.js']
		}
	});
};
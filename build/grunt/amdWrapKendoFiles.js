/* jshint -W117 */
module.exports = function (grunt) {
	'use strict';
	var define;

	grunt.config.set('amdWrapKendoFiles', {
		task: {
			src: '<%= yeoman.app %>/bower_components/kendo-ui/js',
			out: '.tmp/kendo-rs'
		}
	});

	grunt.registerMultiTask('amdWrapKendoFiles',
		'wrap kendo files in amd loader prior to running require js opt',
		function () {
			var done = this.async();
			var kendoFiles = [
				'kendo.autocomplete.min.js', 'kendo.binder.min.js',
				'kendo.calendar.min.js', 'kendo.colorpicker.min.js',
				'kendo.columnmenu.min.js', 'kendo.combobox.min.js',
				'kendo.core.min.js', 'kendo.data.min.js',
				'kendo.data.odata.min.js', 'kendo.data.xml.min.js',
				'kendo.datepicker.min.js', 'kendo.datetimepicker.min.js',
				'kendo.draganddrop.min.js', 'kendo.dropdownlist.min.js',
				'kendo.editable.min.js', 'kendo.editor.min.js',
				'kendo.filtermenu.min.js', 'kendo.fx.min.js',
				'kendo.grid.min.js', 'kendo.groupable.min.js',
				'kendo.imagebrowser.min.js', 'kendo.list.min.js',
				'kendo.listview.min.js', 'kendo.menu.min.js',
				'kendo.multiselect.min.js', 'kendo.numerictextbox.min.js',
				'kendo.pager.min.js', 'kendo.panelbar.min.js',
				'kendo.popup.min.js', 'kendo.reorderable.min.js',
				'kendo.resizable.min.js', 'kendo.router.min.js',
				'kendo.selectable.min.js', 'kendo.slider.min.js',
				'kendo.sortable.min.js', 'kendo.splitter.min.js',
				'kendo.tabstrip.min.js', 'kendo.timepicker.min.js',
				'kendo.tooltip.min.js', 'kendo.treeview.min.js',
				'kendo.upload.min.js', 'kendo.userevents.min.js',
				'kendo.validator.min.js', 'kendo.view.min.js',
				'kendo.window.min.js'
			];

			var sourcePath = this.data.src;
			var destPath = this.data.out;

			function processFiles() {
				var i = -1;
				var l = kendoFiles.length;

				function nextStep() {
					i++;
					if (i < l) {
						var fileName = kendoFiles[i];
						processOne(fileName, nextStep);
					} else {
						console.log('All finished');
						done(true);
					}
				}

				nextStep();
			}

			function processOne(fileName, callback) {
				console.log('Processing: ' + fileName);

				var fullName = sourcePath + '/' + fileName;
				var fs = require('fs');

				fs.readFile(fullName, 'utf8', function (err, data) {
					if (err) {
						console.log(err);
						done(false);
					} else {
						getFileDependencies(fileName, data);
						callback();
					}
				});
			}

			function saveCode(fileName, code, moduleDependencies) {
				var fs = require('fs');

				var moduleDependenciesString = '\'' +
					moduleDependencies.join('\', \'') + '\'';

				var newCode = 'define([' + moduleDependenciesString + '],' +
					'\r\n' + code + '\r\n' + ');';
				fs.writeFile(destPath + '/' + fileName, newCode,
					function (err) {
						if (err) {
							console.log(err);
							done(false);
						} else {
							console.log(fileName + ' was saved!');
						}
					});
			}

			function getFileDependencies(fileName, code) {
				// * This is where the magic happens.
				// the kendo modules call define with the dependencies and
				// the function.
				define = function (moduleDependencies, code) {
					for (var i = 0; i < moduleDependencies.length; i++) {
						var str = moduleDependencies[i];
						str = str.replace('./', 'kendo/');
						moduleDependencies[i] = str;
					}

					// / OPTIONAL STEP
					// / Set this to your jQuery path. If you don't
					// include jQuery globally,
					// / you run the risk of a race condition.
					moduleDependencies.push('jquery');

					console.log('Found dependencies: [' +
						moduleDependencies.join(':') + ']');
					saveCode(fileName, code, moduleDependencies);
				};
				define.amd = true; // Needed to make sure define gets called

				try {
					/* jshint -W061 */
					eval(code);
				} catch (e) {
					// Yes, pokeman error handling...
					// We don't care if the code actually runs,
					// so long as 'define' gets called.
					console.log('err on eval');
					done(false);
				}
			}

			console.log('Starting');
			grunt.file.mkdir(destPath);
			processFiles();
		});
};
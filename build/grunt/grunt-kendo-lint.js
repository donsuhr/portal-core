module.exports = function (grunt) {
	grunt.config.set('kendo_lint', {
		files: [
			'<%= yeoman.app %>/scripts/**/*.js',
			'!<%= yeoman.app %>/scripts/vendor/**'
		]
	});
};
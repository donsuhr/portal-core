module.exports = function (grunt) {
	grunt.config.set('complexity', {
		generic: {
			src: [
				'<%= yeoman.app %>/scripts/**/*.js',
				'!<%= yeoman.app %>/scripts/vendor/**'
			],
			options: {
				//jsLintXML : 'report.xml', // create XML JSLint-like report
				//checkstyleXML : 'checkstyle.xml', // create checkstyle report
				errorsOnly: false, // show only maintainability errors
				cyclomatic: 3,
				halstead: 8,
				maintainability: 75
			}
		}
	});
};
module.exports = function (grunt) {
	grunt.config.set('less', {
		compile: {
			options: {
				compress: false,
				ieCompat: true,
				sourceMap: true,
				sourceMapFilename: '.tmp/styles/<%= pkg.name %>.css.map',
				sourceMapBasepath: '<%= yeoman.app %>/',
				sourceMapRootpath: '/',
				//dumpLineNumbers : 'comments',
				sourceMapURL: '/styles/<%= pkg.name %>.css.map'
			},
			files: {
				'.tmp/styles/<%= pkg.name %>.css': [
					'<%= yeoman.app %>/styles/<%= pkg.name %>.less'
				]
			}
		},
		dist: {
			options: {
				ieCompat: true,
				cleancss: false,
				report: 'min',
				compress: false,
				sourceMap: false
			},
			files: {
				'.tmp/styles/<%= pkg.name %>.css': [
					'<%= yeoman.app %>/styles/<%= pkg.name %>.less'
				]
			}
		}

	});
};
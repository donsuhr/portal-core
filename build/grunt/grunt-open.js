module.exports = function (grunt) {
	grunt.config.set('open', {
		delayForConcurrent: {
			path: 'http://<%= connect.options.hostname %>:<%= connect.options.port %>/'
		}
	});
};
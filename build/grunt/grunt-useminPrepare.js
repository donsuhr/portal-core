module.exports = function (grunt) {
	grunt.config.set('useminPrepare', {
		options: {
			dest: '<%= yeoman.dist %>'
		},
		html: '<%= yeoman.app %>/index.html'
	});
};
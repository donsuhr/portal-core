module.exports = function (grunt) {
	grunt.config.set('jasmine', {
		pivotal: {
			src: 'app/scripts/studentInformation/**/*.js',
			options: {
				//host: 'http://127.0.0.1:8000/',
				keepRunner: false,
				specs: '<%= yeoman.app %>/../test/spec/*Spec.js',
				helpers: ['<%= yeoman.app %>/../test/spec/*Helper.js'],
				template: require('grunt-template-jasmine-requirejs'),
				templateOptions: {
					requireConfigFile: '<%= yeoman.app %>/scripts/common.js',
					requireConfig: {
						baseUrl: './app/scripts',
						callback: function () {
							require(['jasmine-jquery']);
						},
						paths: {
							'jasmine-jquery': '../bower_components/jasmine-jquery/lib/jasmine-jquery'
						},
						shim: {
							'jasmine-jquery': {
								deps: ['jquery']
							}
						}
					}
				}
			}
		}
	});
};
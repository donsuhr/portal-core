module.exports = function (grunt) {
	grunt.config.set('replace', {
		dist: {
			src: ['<%= yeoman.dist %>/index.html'],
			overwrite: true,
			replacements: [
				{
					from: 'scripts\/common',
					to: 'scripts/dist'
				},
				{
					from: 'bower_components\/requirejs\/require.js',
					to: 'scripts/vendor/requirejs-2.1.9/require.min.js'
				},
				{
					from: 'bower_components\/kendo-ui\/styles\/',
					to: 'http://cdn.kendostatic.com/2013.2.716/styles/'
				},
				{
					from: 'styles\/vendor\/bootstrap-2.3.2\/css\/',
					to: '//netdna.bootstrapcdn.com/bootstrap/2.3.2/css/'
				}
			]
		}
	});
};
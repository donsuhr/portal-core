/*
 configure mockjax and mockjson plugins
 */
define(['jquery', 'mockjax', 'mockjson'], function ($) {
	'use strict';
	$('#previewModeNotes').append('mockjaxConfig - jQuery ' + $.fn.jquery + ' loaded!');
});
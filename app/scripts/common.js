require.config({
	baseUrl: 'scripts',
	paths: {
		'components': '../bower_components',
		'jquery': '../bower_components/jquery/jquery',
		'kendo': '../bower_components/kendo-ui/js',
		'mockjax': '../bower_components/jquery-mockjax/jquery.mockjax',
		'mockjson': '../bower_components/mockJSON/js/jquery.mockjson',
		'bootstrap': 'vendor/bootstrap-2.3.2/bootstrap.min',
		'placeholder': '../bower_components/jquery-placeholder/jquery.placeholder.min',
		'text': '../bower_components/requirejs-text/text',
		'templates': '../templates',
		'cmcKendoGrid': '../bower_components/cmcKendoGrid/app/scripts/cmcKendoGrid'
	},
	shim: {
		bootstrap: {
			deps: ['jquery']
		},
		jqueryMigrate: {
			deps: ['jquery']
		},
		placeholder: {
			deps: ['jquery']
		},
		'kendo/kendo.core.min': {
			deps: ['jquery']
		},
		mockjax: {
			deps: ['jquery']
		},
		mockjson: {
			deps: ['jquery']
		}
	}
});

define([], function () {
	'use strict';
	if (typeof window.devmode !== 'undefined' && !window.requireTestMode) {
		require.config({
			deps: ['mockjaxConfig', 'jquery'],
			callback: function () {
				require(['main']);
			}
		});
	}
	if (window.requireTestMode) {
		require.config({
			baseUrl: '../app/scripts'
		});
	}
});
define(
	[
		'jquery', 'portal-core', 'bootstrap', 'kendo/kendo.datetimepicker.min', 'kendo/kendo.tabstrip.min',
		'kendo/kendo.datepicker.min'
	],
	function ($) {
		'use strict';
		$('#datetimepicker').kendoDatePicker({
			value: new Date()
		});
		$('#tabstrip').kendoTabStrip({
			animation: {
				open: {
					effects: 'fadeIn'
				}
			}
		});
		$('#previewModeNotes').append('-- main - jQuery ' + $.fn.jquery + ' loaded!');
	});
